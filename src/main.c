#include <pebble.h>
  
static Window *s_main_window;
static TextLayer *s_time_layer;
static TextLayer *s_weather_layer;
static GFont s_time_font; 
static GFont s_weather_font;

static void main_window_load(Window *window) {
  window_set_background_color(window, GColorClear);

  s_time_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_PERFECT_DOS_48));
  s_weather_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_PERFECT_DOS_20));
  
  s_time_layer = text_layer_create(GRect(5, 52, 139, 50));  
  text_layer_set_background_color(s_time_layer, GColorClear);
  text_layer_set_text_color(s_time_layer, GColorBlack);
  text_layer_set_text(s_time_layer, "00:00");
  text_layer_set_font(s_time_layer, s_time_font);
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);
  
  s_weather_layer = text_layer_create(GRect(0, 130, 144, 25));
  text_layer_set_background_color(s_weather_layer, GColorWhite);
  text_layer_set_text_color(s_weather_layer, GColorBlack);
  text_layer_set_text_alignment(s_weather_layer, GTextAlignmentCenter);
  text_layer_set_text(s_weather_layer, "Loading...");
  
  layer_add_child(window_get_root_layer(s_main_window), text_layer_get_layer(s_time_layer));
  layer_add_child(window_get_root_layer(s_main_window), text_layer_get_layer(s_weather_layer));
}

static void main_window_unload(Window *window) {
  text_layer_destroy(s_time_layer);
  
  fonts_unload_custom_font(s_time_font);
  fonts_unload_custom_font(s_weather_font);
}

static void updateLabel(struct tm *tick_time) {
  static char buffer[] = "00:00";
  
  if (clock_is_24h_style()) {
    strftime(buffer, sizeof(buffer), "%H:%M", tick_time);
  } else {
    strftime(buffer, sizeof(buffer), "%I:%M", tick_time);
  }
  
  text_layer_set_text(s_time_layer, buffer);
}

static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
  updateLabel(tick_time);
}

static void init() {
  s_main_window = window_create();
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload
  });
  
  tick_timer_service_subscribe(SECOND_UNIT, tick_handler);
  
  window_stack_push(s_main_window, true);
  
  time_t temp = time(NULL); 
  struct tm *tick_time = localtime(&temp);
  updateLabel(tick_time);
}

static void deinit() {
  window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}